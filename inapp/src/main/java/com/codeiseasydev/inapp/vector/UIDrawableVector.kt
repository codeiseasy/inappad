package com.codeiseasydev.inapp.vector

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue

import android.util.TypedValue.COMPLEX_UNIT_DIP

class UIDrawableVector(val context: Context?) {
    private val displayMetrics: DisplayMetrics = context!!.resources.displayMetrics
    private val padding = FloatArray(4)

    private var radius: Float = 0f
    private var radiusTop: Float = 0f
    private var radiusBottom: Float = 0f

    private var isMultipleCornerRadius: Boolean = false
    private var isMultipleColors: Boolean = false
    private var bgColor: Int = Color.WHITE
    private var bgColors: IntArray = intArrayOf(Color.WHITE)

    fun padding(all: Float): UIDrawableVector {
        for (i in 0 until 4) {
            this.padding[i] = all
        }
        return this
    }

    fun padding(left: Float, top: Float, right: Float, bottom: Float): UIDrawableVector {
        val padding = arrayOf(left, top, right, bottom)
        for (i in padding.indices) {
            this.padding[i] = padding[i]
        }
        return this
    }

    fun backgroundColor(color: Int): UIDrawableVector {
        this.bgColor = color
        this.isMultipleColors = false
        return this
    }

    fun backgroundColors(colors: IntArray?): UIDrawableVector {
        this.bgColors = colors!!
        this.isMultipleColors = true
        return this
    }

    fun cornerRadius(radius: Float): UIDrawableVector {
        this.radius = radius
        this.isMultipleCornerRadius = false
        return this
    }

    fun cornerRadiusTop(radius: Float): UIDrawableVector {
        this.isMultipleCornerRadius = true
        this.radiusTop = radius
        return this
    }

    fun cornerRadiusTop(radius: Int): UIDrawableVector {
        this.isMultipleCornerRadius = true
        this.radiusTop = (radius.toFloat() / 2)
        return this
    }

    fun cornerRadiusBottom(radius: Float): UIDrawableVector {
        this.isMultipleCornerRadius = true
        this.radiusBottom = radius
        return this
    }

    fun cornerRadiusBottom(radius: Int): UIDrawableVector {
        this.isMultipleCornerRadius = true
        this.radiusBottom = (radius.toFloat() / 2)
        return this
    }

    fun apply(): Drawable {
        val gradientDrawable = GradientDrawable()
        gradientDrawable.gradientType = GradientDrawable.RADIAL_GRADIENT
        gradientDrawable.orientation = GradientDrawable.Orientation.RIGHT_LEFT
        if (isMultipleColors) {
            gradientDrawable.colors = bgColors
        } else {
            gradientDrawable.setColor(bgColor)
        }
        gradientDrawable.setStroke(dimension(0.3f), Color.parseColor("#3B4045"))
        if (!isMultipleCornerRadius) {
            //gradientDrawable.cornerRadius = dimension(radius.toFloat()).toFloat()
            gradientDrawable.cornerRadii = floatArrayOf(
                // top of section
                dimension(radius),
                dimension(radius),
                dimension(radius),
                dimension(radius),

                //Bottom of section
                dimension(radius),
                dimension(radius),
                dimension(radius),
                dimension(radius)
            )
        } else {
            gradientDrawable.cornerRadii = floatArrayOf(
                // top of section
                dimension(radiusTop),
                dimension(radiusTop),
                dimension(radiusTop),
                dimension(radiusTop),

                //Bottom of section
                dimension(radiusBottom),
                dimension(radiusBottom),
                dimension(radiusBottom),
                dimension(radiusBottom)
            )
        }
        val drawables = arrayOf<Drawable>(gradientDrawable)
        val layerDrawable = LayerDrawable(drawables)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layerDrawable.setPadding(
                dimension(padding[0]),
                dimension(padding[1]),
                dimension(padding[2]),
                dimension(padding[3])
            )
        }
        layerDrawable.setLayerInset(0, 0, 0, 0, 0)
        return layerDrawable
    }

    class BorderDrawable {
        private var radius: Float = 0f
        private var backgroundColor: Int = Color.TRANSPARENT
        private var strokeWidth: Int = 1
        private var strokeColor: Int = Color.TRANSPARENT

        fun setStroke(width: Int, color: Int): BorderDrawable {
            this.strokeWidth = width
            this.strokeColor = color
            return this
        }

        fun setStrokeWidth(stroke: Int): BorderDrawable {
            this.strokeWidth = stroke
            return this
        }

        fun setStrokeColor(color: Int): BorderDrawable {
            this.strokeColor = color
            return this
        }

        fun setCornerRadius(radius: Float): BorderDrawable {
            this.radius = radius
            return this
        }

        fun setBackgroundColor(color: Int): BorderDrawable {
            this.backgroundColor = color
            return this
        }

        fun apply(): Drawable {
            val border = GradientDrawable()
            border.setColor(backgroundColor)
            border.cornerRadius = radius
            border.setStroke(strokeWidth, strokeColor)
            return border
        }
    }

    private fun <T> dimension(value: Float): T {
        return TypedValue.applyDimension(COMPLEX_UNIT_DIP, value, displayMetrics) as T
    }

    companion object {
        fun setupShape(context: Context?): UIDrawableVector {
            return UIDrawableVector(context)
        }

        fun setupBorderDrawable(): BorderDrawable {
            return BorderDrawable()
        }
    }
}
