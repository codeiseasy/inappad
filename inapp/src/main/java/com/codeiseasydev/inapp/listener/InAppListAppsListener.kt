package com.codeiseasydev.inapp.listener

import com.codeiseasydev.inapp.model.InAppModel

interface InAppListAppsListener {
    fun onResponse(response: ArrayList<InAppModel>)
    fun onFailure(error: String?)
}