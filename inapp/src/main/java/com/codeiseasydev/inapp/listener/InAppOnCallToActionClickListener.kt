package com.codeiseasydev.inapp.listener

interface InAppOnCallToActionClickListener {
    fun onClick(pkg: (String?))
}