package com.codeiseasydev.inapp.util


import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * Created by Develop on 3/1/2018.
 */

object InAppFontUtil {
    fun overrideFonts(context: Context?, v: View, path: String) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    overrideFonts(context, v.getChildAt(i), path)
                }
            } else if (v is TextView) {
                v.typeface = Typeface.createFromAsset(context!!.assets, path)
            }
        } catch (e: Exception) {
        }
    }

    fun overrideFonts(context: Context, v: View) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    overrideFonts(context, v.getChildAt(i), "ar_font.ttf")
                }
            } else if (v is TextView) {
                v.typeface = Typeface.createFromAsset(context.assets, "ar_font.ttf")
            }
        } catch (e: Exception) {
        }
    }
}
