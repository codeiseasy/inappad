package com.codeiseasydev.inapp.util.animation

import kotlin.math.sin

/**
 * Created by Develop on 2/1/2018.
 */

class InAppInterpolator : android.view.animation.Interpolator {

    private val mCycles = 0.5f

    override fun getInterpolation(input: Float): Float {
        return sin(2.0 * mCycles.toDouble() * Math.PI * input.toDouble()).toFloat()
    }
}