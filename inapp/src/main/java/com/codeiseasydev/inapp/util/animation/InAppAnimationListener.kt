package com.codeiseasydev.inapp.util.animation

interface InAppAnimationListener {
     fun onAnimationStart() = Unit
     fun onAnimationEnd() = Unit
     fun onAnimationCancel() = Unit
}