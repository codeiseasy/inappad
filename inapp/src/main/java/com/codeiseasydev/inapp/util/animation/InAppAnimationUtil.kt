package com.codeiseasydev.inapp.util.animation

import android.view.View
import android.view.animation.Interpolator
import android.view.animation.TranslateAnimation
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorCompat
import androidx.core.view.ViewPropertyAnimatorListener
import java.util.*


class InAppAnimationUtil(private val viewGroup: View?) {
    private var animationViewCompat: ViewPropertyAnimatorCompat? = null
    private var inAppAnimationListener: InAppAnimationListener? = null
    private var interpolator: Interpolator? = null
    private var duration: Int = 0
    private var scaleX: Float = 0.toFloat()
    private var scaleY: Float = 0.toFloat()
    private var filteredView: View? = null
    private var isFilteredView: Boolean = false


    init {
        defaultValues()

    }

    private fun defaultValues() {
        this.interpolator = InAppInterpolator()
        this.duration = 100
        this.scaleX = 0.9f
        this.scaleY = 0.9f
    }

    fun setFilteredView(filteredView: View): InAppAnimationUtil {
        this.filteredView = filteredView
        this.isFilteredView = viewGroup == filteredView
        return this
    }

    fun setDuration(value: Int): InAppAnimationUtil {
        this.duration = value
        return this
    }

    fun scaleX(value: Float): InAppAnimationUtil {
        this.scaleX = value
        return this
    }

    fun scaleY(value: Float): InAppAnimationUtil {
        this.scaleY = value
        return this
    }

    fun setInterpolator(value: Interpolator): InAppAnimationUtil {
        this.interpolator = value
        return this
    }

    fun commit(listenerInApp: InAppAnimationListener): InAppAnimationUtil {
        this.inAppAnimationListener = listenerInApp
        if (!isFilteredView) {
            animationViewCompat = ViewCompat.animate(viewGroup!!)
                .setDuration(duration.toLong())
                .scaleX(scaleX)
                .scaleY(scaleY)
                .setInterpolator(interpolator)
                .setListener(object : ViewPropertyAnimatorListener {
                    override fun onAnimationStart(view: View) {
                        inAppAnimationListener?.onAnimationStart()
                    }

                    override fun onAnimationEnd(view: View) {
                        cancel()
                        inAppAnimationListener?.onAnimationEnd()
                    }

                    override fun onAnimationCancel(view: View) {
                        inAppAnimationListener?.onAnimationCancel()
                    }
                })
        } else {
            cancel()
            inAppAnimationListener?.onAnimationEnd()
        }
        return this
    }

    fun cancel() {
        animationViewCompat?.cancel()
    }

    companion object {

        fun with(view: View?): InAppAnimationUtil {
            return InAppAnimationUtil(view)
        }

        private fun randInt(i: Int, i2: Int): Int {
            return Random().nextInt(i2 - i + 1) + i
        }

        // slide the view from below itself to the current position
        fun slideUp(view: View) {
            view.visibility = View.VISIBLE
            val animate = TranslateAnimation(
                0f,  // fromXDelta
                0f,  // toXDelta
                view.height.toFloat(),  // fromYDelta
                0f
            ) // toYDelta
            animate.duration = 500
            animate.fillAfter = true
            view.startAnimation(animate)
        }

        // slide the view from its current position to below itself
        fun slideDown(view: View) {
            val animate = TranslateAnimation(
                0f,  // fromXDelta
                0f,  // toXDelta
                0f,  // fromYDelta
                view.height.toFloat()
            ) // toYDelta
            animate.duration = 500
            animate.fillAfter = true
            view.startAnimation(animate)
        }
    }

}
