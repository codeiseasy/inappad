package com.codeiseasydev.inapp.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

object InAppContextCompat {
    fun getString(context: Context?, @StringRes resId: Int): String{
        return context!!.resources.getString(resId)
    }

    fun getColor(context: Context?, @ColorRes color: Int): Int{
        return ContextCompat.getColor(context!!, color)
    }

    fun getDrawable(context: Context?, @DrawableRes drawableRes: Int): Drawable? {
        return ContextCompat.getDrawable(context!!, drawableRes)
    }

    fun getDimensionToPixel(context: Context?, @DimenRes dimenRes: Int): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, context!!.resources.getDimension(dimenRes), context!!.resources.displayMetrics) / 3
    }
}