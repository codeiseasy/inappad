package com.codeiseasydev.inapp.util

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView


import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.text.DecimalFormat
import kotlin.math.floor
import kotlin.math.log10
import kotlin.math.pow

/**
 * Created by Develop on 8/22/2017.
 */

object InAppUtils {

    val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            //enableLoginBtn();
        }
    }


    fun hidenStatusBar(context: Activity) {
        context.requestWindowFeature(
            Window.FEATURE_NO_TITLE
        )
        context.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }


    fun changeStatusBarColor(context: Activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = context.window
            window.addFlags(Integer.MIN_VALUE)
            window.statusBarColor = 0
            window.decorView.systemUiVisibility = 1280
        }
    }


    fun deepChangeTextColor(ctx: Context, parentLayout: ViewGroup, color: Int) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            if (view is TextView) {
                view.textSize = 16f
                //textView.setShadowLayer(1.0f, -1.0f, -1.0f, Color.DKGRAY);
                view.setTextColor(color)
                InAppFontUtil.overrideFonts(ctx, view, "ar_font.ttf")
            } else if (view is Button) {
                view.textSize = 16f
                //button.setShadowLayer(1.0f, -1.0f, -1.0f, Color.DKGRAY);
                view.setTextColor(color)
                InAppFontUtil.overrideFonts(ctx, view, "ar_font.ttf")
            } else if (view is ViewGroup) {
                deepChangeTextColor(ctx, view, color)
            }
        }
    }

    fun deepChangeTextColor(ctx: Context, parentLayout: ViewGroup, color: Int, font: String) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            if (view is TextView) {
                view.textSize = 16f
                //textView.setShadowLayer(1.0f, -1.0f, -1.0f, Color.DKGRAY);
                view.setTextColor(color)
                InAppFontUtil.overrideFonts(ctx, view, font)
            } else if (view is Button) {
                view.textSize = 16f
                //button.setShadowLayer(1.0f, -1.0f, -1.0f, Color.DKGRAY);
                view.setTextColor(color)
                InAppFontUtil.overrideFonts(ctx, view, font)
            } else if (view is ViewGroup) {
                deepChangeTextColor(ctx, view, color, font)
            }
        }
    }

    fun deepChangeTextColor(ctx: Context?, parentLayout: ViewGroup, font: String) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            if (view is TextView) {
                //view.textSize = 16f
                InAppFontUtil.overrideFonts(ctx, view, font)
            } else if (view is Button) {
                // view.textSize = 16f
                InAppFontUtil.overrideFonts(ctx, view, font)
            } else if (view is ViewGroup) {
                deepChangeTextColor(ctx, view, font)
            }
        }
    }


    fun deepChangeTextColor(ctx: Context?, parentLayout: ViewGroup, font: String, textSize: Float) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            if (view is TextView) {
                //view.textSize = textSize
                InAppFontUtil.overrideFonts(ctx, view, font)
            } else if (view is Button) {
                //view.textSize = textSize
                InAppFontUtil.overrideFonts(ctx, view, font)
            } else if (view is ViewGroup) {
                deepChangeTextColor(ctx, view, font)
            }
        }
    }

    fun deepChangeTextColor(ctx: Context, parentLayout: ViewGroup) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            if (view is TextView) {
                view.textSize = 16f
                InAppFontUtil.overrideFonts(ctx, view, "ar_font.ttf")
            } else if (view is Button) {
                view.textSize = 16f
                InAppFontUtil.overrideFonts(ctx, view, "ar_font.ttf")
            } else if (view is ViewGroup) {
                deepChangeTextColor(ctx, view)
            }
        }
    }

    fun deepChangeButtonTextColor(ctx: Context, parentLayout: ViewGroup) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            if (view is Button) {
                view.textSize = 16f
                view.setShadowLayer(1.0f, -1.0f, -1.0f, Color.DKGRAY)
                view.setTextColor(ctx.resources.getColor(android.R.color.white))
                InAppFontUtil.overrideFonts(ctx, view, "ar_font.ttf")
            } else if (view is ViewGroup) {
                deepChangeButtonTextColor(ctx, view)
            }
        }
    }


    fun changeTextStyle(ctx: Context, fatherView: ViewGroup) {
        val view = fatherView.getChildAt(0)
        if (view is TextView) {
            view.textSize = 16f
            view.setShadowLayer(1.0f, -1.0f, -1.0f, Color.DKGRAY)
            view.setTextColor(ctx.resources.getColor(android.R.color.white))
            InAppFontUtil.overrideFonts(ctx, view, "ar_font.ttf")
        }
    }


    fun getBitmapFromURL(src: String): Bitmap? {
        try {
            Log.e("src", src)
            val url = URL(src)
            val connection = url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            val myBitmap = BitmapFactory.decodeStream(input)
            InAppLogDebug.e("Bitmap", "returned")
            return myBitmap
        } catch (e: IOException) {
            e.printStackTrace()
            InAppLogDebug.e("Exception", e.message)
            return null
        }
    }


    fun dimension(ctx: Context?, value: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value.toFloat(),
            ctx!!.resources.displayMetrics
        )
    }

    fun makeLinks(textView: TextView, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
        val spannableStrig = SpannableString(textView.text)
        for (i in links.indices) {
            val clickableSpan = clickableSpans[i]
            val link = links[i]
            val startIndexOfLink = textView.text.toString().indexOf(link)
            spannableStrig.setSpan(
                clickableSpan,
                startIndexOfLink,
                startIndexOfLink + link.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        textView.setHintTextColor(Color.TRANSPARENT)
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(spannableStrig, TextView.BufferType.SPANNABLE)
    }


    fun isHttpOrHttpsUrl(url: String): Boolean {
        val patter = "^(http|https|ftp)://.*$"
        return url.matches(patter.toRegex())
    }


    fun copyStream(inputStream: InputStream, outputStream: OutputStream) {
        val bufferSize = 1024
        try {
            val bytes = ByteArray(bufferSize)
            while (true) {
                val count = inputStream.read(bytes, 0, bufferSize)
                if (count == -1)
                    break
                outputStream.write(bytes, 0, count)
            }
        } catch (ex: Exception) {
        }

    }

    fun setViewMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p = view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }


    /**
     * This sets the maximum length in characters of an EditText view. Since the
     * max length must be done with a filter, this method gets the current
     * filters. If there is already a length filter in the view, it will replace
     * it, otherwise, it will add the max length filter preserving the other
     *
     * @param view
     * @param length
     */
    fun setMaxLength(view: EditText, length: Int) {
        val curFilters: Array<InputFilter>?
        val lengthFilter: InputFilter.LengthFilter
        var idx: Int

        lengthFilter = InputFilter.LengthFilter(length)

        curFilters = view.filters
        if (curFilters != null) {
            idx = 0
            while (idx < curFilters.size) {
                if (curFilters[idx] is InputFilter.LengthFilter) {
                    curFilters[idx] = lengthFilter
                    return
                }
                idx++
            }

            // since the length filter was not part of the list, but
            // there are filters, then add the length filter
            val newFilters = arrayOfNulls<InputFilter>(curFilters.size + 1)
            System.arraycopy(curFilters, 0, newFilters, 0, curFilters.size)
            newFilters[curFilters.size] = lengthFilter
            view.filters = newFilters
        } else {
            view.filters = arrayOf<InputFilter>(lengthFilter)
        }
    }


    fun openUrl(context: Context, url: String) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        } catch (exception: ActivityNotFoundException) {
            exception.printStackTrace()
        }

    }


    fun openAppInPlayStore(ctx: Context?, pkg: String?) {
        try {
            ctx?.startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$pkg")).addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK
                )
            )
        } catch (e: ActivityNotFoundException) {
            ctx?.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$pkg")
                ).addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK
                )
            )
            e.printStackTrace()
        }
    }
    
    fun getFormattedNumber(number: Number): String? {
        val suffix = charArrayOf(' ', 'k', 'M', 'B', 'T', 'P', 'E')
        val numValue: Long = number.toLong()
        val value = floor(log10(numValue.toDouble())).toInt()
        val base = value / 3
        return if (value >= 3 && base < suffix.size) {
            DecimalFormat("#0.0").format(numValue / 10.0.pow(base * 3.toDouble())) + suffix[base]
        } else {
            DecimalFormat("#,##0").format(numValue)
        }
    }

    fun numberFormatter(number: Int): String {
        var numberString = ""
        numberString = if (Math.abs(number / 1000000) > 1) {
            (number / 1000000).toString() + "m"
        } else if (Math.abs(number / 1000) > 1) {
            (number / 1000).toString() + "k"
        } else {
            number.toString()
        }
        return numberString
    }
}
