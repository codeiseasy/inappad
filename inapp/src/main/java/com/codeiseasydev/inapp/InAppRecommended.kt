package com.codeiseasydev.inapp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import com.codeiseasydev.inapp.theme.InAppRecommendedStyles
import com.codeiseasydev.inapp.util.InAppLogDebug
import com.codeiseasydev.inapp.util.InAppUtils
import com.nostra13.universalimageloader.cache.disc.DiskCache
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class InAppRecommended : RelativeLayout, ImageLoadingListener {
    private var inflater: LayoutInflater? = null
    private var rdBackgroundRes: Int = -1
    private var rdUseCompatPadding: Boolean = false
    private var rdCornerOverlap: Boolean = false
    private var rdCorner: Float = 0f
    private var rdElevation: Float = 0f
    private var rdIsAnimation: Boolean = false
    private var rdAnimationAccentColor: Int = Color.BLACK

    private var llInAppAnimationView: View? = null
    private var cvInAppContainer: CardView? = null
    private var llInAppContainer: RelativeLayout? = null
    private var llInAppContent: LinearLayout? = null
    private var ivInAppIcon: ImageView? = null
    private var pbInAppPreLoaded: ProgressBar? = null
    private var tvInAppName: TextView? = null
    private var tvInAppDeveloper: TextView? = null
    private var tvInAppPrice: TextView? = null
    private var tvInAppInstalls: TextView? = null
    private var tvInAppAdBadge: TextView? = null
    private var rbInAppReview: RatingBar? = null
    private var btnInAppInstall: Button? = null
    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null
    private var inAppRecommendedStyles: InAppRecommendedStyles? = null
    private var imageLoader: ImageLoader? = null

    constructor(context: Context) : super(context) {
        init(context, null, 0, 0)
        isInEditMode
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, 0, 0)
        isInEditMode
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs, defStyleAttr, 0)
        isInEditMode
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs, defStyleAttr, defStyleRes)
        isInEditMode
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {
        this.inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var typedArray = context.obtainStyledAttributes(
            attrs,
            R.styleable.InAppRecommended,
            defStyleAttr,
            defStyleRes
        )
        try {
            rdBackgroundRes =
                typedArray.getColor(R.styleable.InAppRecommended_inapp_rd_background, -1)
            rdUseCompatPadding = typedArray.getBoolean(
                R.styleable.InAppRecommended_inapp_rd_use_compat_padding,
                false
            )
            rdCornerOverlap =
                typedArray.getBoolean(R.styleable.InAppRecommended_inapp_rd_corner_overlap, false)
            rdElevation = typedArray.getFloat(R.styleable.InAppRecommended_inapp_rd_elevation, -1f)
            rdCorner = typedArray.getDimension(R.styleable.InAppRecommended_inapp_rd_corner, -1f)
            rdIsAnimation =
                typedArray.getBoolean(R.styleable.InAppRecommended_inapp_rd_animation, false)
            rdAnimationAccentColor = typedArray.getColor(
                R.styleable.InAppRecommended_inapp_rd_animation_accent_color,
                -1
            )
        } finally {
            typedArray.recycle()
        }

        initViews()
        defaultAttrs()
    }

    private fun initViews() {
        inflater?.inflate(R.layout.inapp_vertical_items, this, true)
        llInAppAnimationView = findViewById(R.id.inapp_animation_view)
        llInAppContent = findViewById(R.id.inapp_ll_content)

        cvInAppContainer = findViewById(R.id.inapp_cv_container)
        llInAppContainer = findViewById(R.id.inapp_ll_container)
        ivInAppIcon = findViewById(R.id.inpp_iv_icon)
        pbInAppPreLoaded = findViewById(R.id.inapp_pb_preloading)
        tvInAppName = findViewById(R.id.inpp_tv_name)
        tvInAppDeveloper = findViewById(R.id.inapp_tv_developer)
        tvInAppPrice = findViewById(R.id.inapp_tv_price)
        tvInAppInstalls = findViewById(R.id.inapp_tv_installs)
        tvInAppAdBadge = findViewById(R.id.inapp_tv_badge)
        rbInAppReview = findViewById(R.id.inapp_rb_review)
        btnInAppInstall = findViewById(R.id.inapp_call_to_action)
        avLoadingIndicatorView = findViewById(R.id.inapp_avl_indicator)

        avLoadingIndicatorView?.setIndicatorColor(rdAnimationAccentColor)
        avLoadingIndicatorView?.setIndicator(loadingAnimationType[Random().nextInt(loadingAnimationType.size)])
        viewsVisibility()
    }

    override fun onLoadingStarted(imageUri: String?, view: View?) {}

    override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
        pbInAppPreLoaded?.visibility = View.GONE
    }

    override fun onLoadingCancelled(imageUri: String?, view: View?) {}

    override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {
        pbInAppPreLoaded?.visibility = View.GONE
    }

    private fun viewsVisibility() {
        if (rdIsAnimation) {
            llInAppAnimationView?.visibility = View.VISIBLE
            llInAppContent?.visibility = View.INVISIBLE
            return
        }
        llInAppAnimationView?.visibility = View.GONE
        llInAppContent?.visibility = View.VISIBLE
    }

    private fun defaultAttrs() {
        cvInAppContainer?.setCardBackgroundColor(rdBackgroundRes)
        cvInAppContainer?.useCompatPadding = rdUseCompatPadding
        cvInAppContainer?.cardElevation = rdElevation
        cvInAppContainer?.preventCornerOverlap = rdCornerOverlap
        cvInAppContainer?.radius = rdCorner
    }

    fun setIcon(url: String?) {
        imageLoader = ImageLoader.getInstance()
        imageLoader?.setDefaultLoadingListener(this)
        imageLoader?.init(ImageLoaderConfiguration.createDefault(context))
        var options = DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .bitmapConfig(Bitmap.Config.ARGB_8888)
            .build()


        imageLoader?.displayImage(url, ivInAppIcon, options)
    }

    fun setIcon(drawable: Drawable) {
        ivInAppIcon?.setImageDrawable(drawable)
        if (pbInAppPreLoaded!!.visibility == View.VISIBLE) pbInAppPreLoaded?.visibility = View.GONE
    }

    fun setIcon(bitmap: Bitmap) {
        ivInAppIcon?.setImageBitmap(bitmap)
        if (pbInAppPreLoaded!!.visibility == View.VISIBLE) pbInAppPreLoaded?.visibility = View.GONE
    }

    fun setIcon(resId: Int) {
        ivInAppIcon?.setImageResource(resId)
        if (pbInAppPreLoaded!!.visibility == View.VISIBLE) pbInAppPreLoaded?.visibility = View.GONE
    }

    fun setName(name: String?) {
        tvInAppName?.text = name.toString()
    }

    fun setDeveloper(developer: String?) {
        tvInAppDeveloper?.text = developer.toString()
    }

    fun setPrice(price: String?) {
        tvInAppPrice?.text = price.toString()
    }

    fun setReview(rating: Float) {
        rbInAppReview?.rating = rating
    }

    fun setCallToAction(action: String?) {
        btnInAppInstall?.text = action.toString()
    }

    fun setInstalls(number: Number) {
        tvInAppInstalls?.text = InAppUtils.getFormattedNumber(number)!!.format(Locale.ENGLISH, null)
        //tvInAppInstalls?.textLocale = Locale.ENGLISH
    }

    fun setStyles(styles: InAppRecommendedStyles?) {
        this.inAppRecommendedStyles = styles
        applyStyles()
    }

    private fun applyStyles() {
        val nameTypeface = inAppRecommendedStyles!!.getRecommendedNameTextTypeface()
        val developerTypeface = inAppRecommendedStyles!!.getRecommendedDeveloperTextTypeface()
        val priceTypeface = inAppRecommendedStyles!!.getRecommendedPriceTextTypeface()
        val badgeTypeface = inAppRecommendedStyles!!.getRecommendedBadgeTextTypeface()
        val callToActionTypeface = inAppRecommendedStyles!!.getRecommendedCallToActionTextTypeface()

        val nameTextSize = inAppRecommendedStyles!!.getRecommendedNameTextSize()
        val developerTextSize = inAppRecommendedStyles!!.getRecommendedDeveloperTextSize()
        val priceTextSize = inAppRecommendedStyles!!.getRecommendedPriceTextSize()
        val badgeTextSize = inAppRecommendedStyles!!.getRecommendedBadgeTextSize()
        val callToActionTextSize = inAppRecommendedStyles!!.getRecommendedCallToActionTextSize()

        val nameTextColor = inAppRecommendedStyles!!.getRecommendedNameTextColor()
        val developerTextColor = inAppRecommendedStyles!!.getRecommendedDeveloperTextColor()
        val priceTextColor = inAppRecommendedStyles!!.getRecommendedPriceTextColor()
        val badgeTextColor = inAppRecommendedStyles!!.getRecommendedBadgeTextColor()
        val callToActionTextColor = inAppRecommendedStyles!!.getRecommendedCallToActionTextColor()
        val callToActionBackground = inAppRecommendedStyles!!.getRecommendedCallToActionBackground()

        tvInAppName?.typeface = nameTypeface
        tvInAppName?.textSize = nameTextSize
        tvInAppName?.setTextColor(nameTextColor)

        tvInAppDeveloper?.typeface = developerTypeface
        tvInAppDeveloper?.textSize = developerTextSize
        tvInAppDeveloper?.setTextColor(developerTextColor)

        tvInAppPrice?.typeface = priceTypeface
        tvInAppPrice?.textSize = priceTextSize
        tvInAppPrice?.setTextColor(priceTextColor)

        tvInAppAdBadge?.typeface = badgeTypeface
        tvInAppAdBadge?.textSize = badgeTextSize
        tvInAppAdBadge?.setTextColor(badgeTextColor)

        btnInAppInstall?.typeface = callToActionTypeface
        btnInAppInstall?.textSize = callToActionTextSize
        btnInAppInstall?.setTextColor(callToActionTextColor)
        btnInAppInstall?.background = callToActionBackground
    }

    fun setOnRecommendedClockListener(listener: OnClickListener?) {
        llInAppContainer?.setOnClickListener(listener)
    }

    fun setOnCallToActionClickListener(listener: OnClickListener?) {
        btnInAppInstall?.setOnClickListener(listener)
    }

    fun apply() {
        rdIsAnimation = false
        viewsVisibility()
    }

    fun setAnimation(animation: Boolean) {
        rdIsAnimation = animation
        viewsVisibility()
    }

    companion object {
        private val loadingAnimationType = arrayOf(
            "SquareSpinIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallClipRotatePulseIndicator",
            "BallSpinFadeLoaderIndicator",
            "PacmanIndicator",
            "BallClipRotatePulseIndicator",
            "LineScalePartyIndicator"
        )
    }
}