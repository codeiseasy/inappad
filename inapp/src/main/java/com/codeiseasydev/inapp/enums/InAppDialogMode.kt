package com.codeiseasydev.inapp.enums

enum class InAppDialogMode {
    NORMAL,
    SHEET_BOTTOM
}