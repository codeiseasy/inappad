package com.codeiseasydev.inapp

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Handler

import android.view.Gravity
import android.view.View

import com.codeiseasydev.inapp.adapter.InAppVerticalAdapter
import com.codeiseasydev.inapp.client.InAppHttpClientCallback
import com.codeiseasydev.inapp.listener.InAppOnCallToActionClickListener

import com.codeiseasydev.inapp.model.InAppModel

import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.setPadding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasydev.inapp.client.InAppPathSegments
import com.codeiseasydev.inapp.client.InAppRequestServerClient
import com.codeiseasydev.inapp.enums.InAppDialogMode
import com.codeiseasydev.inapp.listener.InAppListAppsListener
import com.codeiseasydev.inapp.theme.InAppRecommendedStyles
import com.codeiseasydev.inapp.util.*

import com.codeiseasydev.inapp.util.InAppUtils.dimension
import com.codeiseasydev.inapp.vector.UIDrawableVector
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog


class InAppListApps {
    private var mContext: Context? = null
    private var mServerUrl: String? = null
    private lateinit var mPathSegments: Array<out String>
    private var mQueryName: String? = ""
    private var mQueryValue: String? = ""
    private var isDebugging: Boolean = false
    private var mCallToActionClickListenerInApp: InAppOnCallToActionClickListener? = null
    private var mInAppListAppsListener: InAppListAppsListener? = null
    private var mStyles: InAppRecommendedStyles? = null
    private var mTitleTypeFace: Typeface? = null
    private var mTitleTextSize: Float = -1f

    constructor(context: Context?, url: String?) {
        this.mContext = context
        this.mServerUrl = url
    }

    constructor(context: Context?) {
        this.mContext = context
    }

    fun setUrl(url: String?): InAppListApps {
        this.mServerUrl = url
        return this
    }

    fun setTitleTypeFace(fontPath: String?): InAppListApps {
        this.mTitleTypeFace = Typeface.createFromAsset(mContext!!.assets, fontPath.toString())
        return this
    }

    fun setTitleTypeFace(font: Int): InAppListApps {
        this.mTitleTypeFace = ResourcesCompat.getFont(mContext!!, font)
        return this
    }

    fun setTitleTextSize(size: Float): InAppListApps {
        this.mTitleTextSize = size
        return this
    }

    fun setStyles(styles: InAppRecommendedStyles?): InAppListApps {
        this.mStyles = styles
        return this
    }

    fun setDebugging(debug: Boolean) : InAppListApps{
        isDebugging = debug
        return this
    }

    fun setPath(path: String?): InAppListApps {
        this.mPathSegments = arrayOf(path.toString())
        return this
    }

    fun setPaths(vararg path: String): InAppListApps {
        this.mPathSegments = path
        return this
    }

    fun addListener(listener: InAppListAppsListener): InAppListApps {
        this.mInAppListAppsListener = listener
        return this
    }

    fun setQuery(query: String?, value: String?): InAppListApps {
        this.mQueryName = query
        this.mQueryValue = value
        return this
    }

    fun build(modeInApp: InAppDialogMode){
        var containerLayout = LinearLayout(this.mContext)
        var containerLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        containerLayout.layoutParams = containerLayoutParams
        containerLayout.orientation = LinearLayout.VERTICAL
        containerLayout.gravity = Gravity.CENTER
        containerLayout.background = UIDrawableVector(this.mContext)
            .backgroundColor(ContextCompat.getColor(this.mContext!!, R.color.inapp_dialog_background_color))
            .cornerRadiusTop(mContext!!.resources.getDimensionPixelSize(R.dimen.inapp_bottom_dialog_corner))
            .apply()

        var pbLoading = ProgressBar(this.mContext)
        var pbLoadingParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dimension(this.mContext,50).toInt())
        pbLoadingParams.leftMargin = 50
        pbLoadingParams.rightMargin = 50
        pbLoadingParams.bottomMargin = 50
        pbLoadingParams.topMargin = 50
        pbLoading.layoutParams = pbLoadingParams


        var tvDialogTitle = TextView(this.mContext)
        var tvDialogTitleParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        tvDialogTitle.layoutParams = tvDialogTitleParams
        tvDialogTitle.background = UIDrawableVector(this.mContext)
            .backgroundColor(ContextCompat.getColor(this.mContext!!, R.color.inapp_dialog_title_bg_color))
            .cornerRadiusTop(mContext!!.resources.getDimensionPixelSize(R.dimen.inapp_bottom_dialog_corner))
            .apply()

        if (mTitleTextSize != -1f){
            tvDialogTitle.textSize = mTitleTextSize
        }
        tvDialogTitle.setTextColor(InAppContextCompat.getColor(this.mContext, R.color.inapp_dialog_title_text_color))
        tvDialogTitle.text = InAppContextCompat.getString(this.mContext, R.string.inapp_list_apps_dialog_label)
        if (mTitleTypeFace != null){
            tvDialogTitle.typeface = mTitleTypeFace
        }
        tvDialogTitle.gravity = Gravity.CENTER_HORIZONTAL
        tvDialogTitle.visibility = View.GONE
        tvDialogTitle.setPadding(dimension(this.mContext,15).toInt())

        var layoutConnectionError = LinearLayout(this.mContext)
        var layoutConnectionErrorParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutConnectionErrorParams.bottomMargin = 50
        layoutConnectionErrorParams.topMargin = 50
        layoutConnectionError.layoutParams = layoutConnectionErrorParams
        layoutConnectionError.gravity = Gravity.CENTER_VERTICAL

        var ivConnectionError = ImageView(this.mContext)
        var ivConnectionErrorParams = LinearLayout.LayoutParams(dimension(this.mContext,80).toInt(), LinearLayout.LayoutParams.WRAP_CONTENT)
        ivConnectionError.layoutParams = ivConnectionErrorParams
        ivConnectionError.setImageResource(R.drawable.inapp_ic_wifi_error)

        var tvConnectionError = TextView(this.mContext)
        var tvConnectionErrorParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        tvConnectionError.layoutParams = tvConnectionErrorParams
        tvConnectionError.textSize = 16f

        var recyclerView = RecyclerView(this.mContext!!)
        var recyclerViewParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        recyclerView.layoutParams = recyclerViewParams

        containerLayout.addView(tvDialogTitle)
        containerLayout.addView(pbLoading)
        layoutConnectionError.addView(ivConnectionError)
        layoutConnectionError.addView(tvConnectionError)
        containerLayout.addView(layoutConnectionError)
        containerLayout.addView(recyclerView)

        var storeAppsAdapter = InAppVerticalAdapter(this.mContext, mStyles)
        recyclerView.layoutManager = LinearLayoutManager(this.mContext)
        recyclerView.adapter = storeAppsAdapter
        storeAppsAdapter.setOnButtonInstallClickListener(object : InAppOnCallToActionClickListener {
            override fun onClick(pkg: String?) {
                InAppUtils.openAppInPlayStore(mContext, pkg)
                if(!isDebugging){
                    InAppRequestServerClient.builder(mContext)
                        .setUrl(InAppConsts.baseURL)
                        .setPaths(*InAppPathSegments.pathSegmentAppsClicks)
                        .identifier(pkg)
                        .clicks()
                }
            }
        })
        if (InAppCheckNetwork.getInstance(mContext!!).isOnline){
            layoutConnectionError.visibility = View.GONE
            InAppRequestServerClient.builder(mContext)
                .setUrl(mServerUrl)
                .setPaths(*mPathSegments)
                .fetchAll(object : InAppHttpClientCallback<ArrayList<InAppModel>> {
                    override fun onResponseSuccess(apps: ArrayList<InAppModel>) {
                        pbLoading.visibility = View.GONE
                        tvDialogTitle.visibility = View.VISIBLE
                        storeAppsAdapter.setItems(apps)
                        mInAppListAppsListener?.onResponse(apps)
                        if(!isDebugging){
                            for (app in apps){
                                if (app.available && app.packageName != mContext!!.packageName){
                                    InAppRequestServerClient.builder(mContext)
                                        .setUrl(InAppConsts.baseURL)
                                        .setPaths(*InAppPathSegments.pathSegmentAppsViews)
                                        .identifier(app.packageName)
                                        .views()
                                    InAppLogDebug.d("RESPONSE_SUCCESS", app.packageName.toString())
                                }
                            }
                        }
                    }

                    override fun onResponseError(error: String?) {
                        InAppLogDebug.d("onResponseError", error.toString())
                        pbLoading.visibility = View.GONE
                        mInAppListAppsListener?.onFailure(error.toString())
                    }
                })
        } else {
            tvConnectionError.text = InAppContextCompat.getString(this.mContext, R.string.inapp_error_connection)
            layoutConnectionError.visibility = View.VISIBLE
            pbLoading.visibility = View.GONE
        }
        //InAppUtils.deepChangeTextColor(this.context, containerLayout as ViewGroup, InAppContextCompat.getString(this.context, R.string.inapp_text_default_font), 9f)
        when(modeInApp){
            InAppDialogMode.NORMAL -> {
                var dialog = Dialog(this.mContext!!, R.style.inapp_MyDialogTheme)
                dialog.setContentView(containerLayout)
                dialog.window!!.attributes.windowAnimations = R.style.inapp_DialogAnimation
                dialog.show()
            }
            InAppDialogMode.SHEET_BOTTOM -> {
                var bottomSheetDialog = BottomSheetDialog(this.mContext!!, R.style.inapp_BottomSheetDialogTheme)
                bottomSheetDialog.setContentView(containerLayout)
                bottomSheetDialog.show()
                bottomSheetDialog.setOnShowListener { dialog ->
                    Handler().postDelayed(Runnable {
                        val d = dialog as BottomSheetDialog
                        val bottomSheet = d.findViewById<FrameLayout>(R.id.design_bottom_sheet)
                        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    }, 0)
                }
            }
        }

    }

    fun setOnCallToActionClickListener(listenerInApp: InAppOnCallToActionClickListener?) {
        mCallToActionClickListenerInApp = listenerInApp
    }

    companion object {

        fun with(context: Context): InAppListApps{
            return InAppListApps(context)
        }

        fun with(context: Context, url: String?): InAppListApps{
            return InAppListApps(context, url)
        }
    }
}