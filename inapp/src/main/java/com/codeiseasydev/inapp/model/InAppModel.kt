package com.codeiseasydev.inapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InAppModel {

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("package")
    @Expose
    var packageName: String? = null

    @SerializedName("icon")
    @Expose
    var icon: String? = null

    @SerializedName("version_name")
    @Expose
    var versionName: String? = null

    @SerializedName("version_code")
    @Expose
    var versionCode: Int = 0

    @SerializedName("min_sdk_version")
    @Expose
    var minSdkVersion: Int = 0

    @SerializedName("target_sdk_version")
    @Expose
    var targetSdkVersion: Int = 0

    @SerializedName("size")
    @Expose
    var size: Double = 0.0

    @SerializedName("category")
    @Expose
    var category: String? = null

    @SerializedName("developer_name")
    @Expose
    var developer: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("content_rating")
    @Expose
    var contentRating: Int = 0

    @SerializedName("ratings")
    @Expose
    var appContentRating: Int = 0

    @SerializedName("rated_stars")
    @Expose
    var ratedStars: Double = 0.0

    @SerializedName("installs")
    @Expose
    var installs: Int = 0

    @SerializedName("short_description")
    @Expose
    var shortDescription: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("screenshots")
    @Expose
    var screenshots: Array<String>? = null

    @SerializedName("recommended")
    @Expose
    var recommended: Boolean = false

    @SerializedName("available")
    @Expose
    var available: Boolean = false

    @SerializedName("created_at")
    @Expose
    var createdAt: Double = 0.0

    @SerializedName("updated")
    @Expose
    var appUpdated: Double = 0.0

    @SerializedName("token")
    @Expose
    var token: String? = null
}