package com.codeiseasydev.inapp.adapter

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasydev.inapp.R
import com.codeiseasydev.inapp.listener.InAppOnCallToActionClickListener
import com.codeiseasydev.inapp.model.InAppModel
import com.codeiseasydev.inapp.util.InAppLogDebug
import com.codeiseasydev.inapp.util.InAppUtils
import com.codeiseasydev.inapp.util.animation.InAppAnimationListener
import com.codeiseasydev.inapp.util.animation.InAppAnimationUtil
import com.codeiseasydev.inapp.view.InAppBadgeOval
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener
import xyz.schwaab.avvylib.AvatarView


open class InAppHorizontalAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private var context: Context? = null
    private var layoutResId: Int = R.layout.inapp_horizontal_items
    private var inAppModel: ArrayList<InAppModel> = ArrayList()
    private var imageLoader: ImageLoader? = null
    private var options: DisplayImageOptions? = null
    private var callToActionClickListenerInApp: InAppOnCallToActionClickListener? = null


    constructor(context: Context?)  : super() {
        this.context = context
        init()
    }

    constructor(context: Context?, @LayoutRes resId: Int)  : super() {
        this.context = context
        this.layoutResId = resId
        init()
    }

    private fun init(){

        imageLoader = ImageLoader.getInstance()
        imageLoader?.init(ImageLoaderConfiguration.createDefault(context))
        options = DisplayImageOptions.Builder()
            //.showImageOnLoading(R.drawable.stat_notify_error)
            //.showImageForEmptyUri(R.drawable.stat_notify_error)
            //.showImageOnFail(R.drawable.stat_notify_error)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .build()
    }

   inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ImageLoadingListener {
        internal var civInAppIcon: AvatarView = itemView.findViewById(R.id.av_inapp_icon)
        internal var tvAppName: TextView = itemView.findViewById(R.id.inpp_tv_name)
        internal var tvAdBadge: TextView = itemView.findViewById(R.id.inapp_tv_civ_badge)

        init {
            InAppUtils.deepChangeTextColor(itemView.context, itemView as ViewGroup, "ar_font.ttf", 9f)
            tvAdBadge.background = InAppBadgeOval(itemView.context)
                .setBadgeBackgroundColor(ContextCompat.getColor(itemView.context, R.color.inapp_civ_badge_bg_color))
                .setStrokeWidth(2f)
                .setStrokeColors(intArrayOf(
                    ContextCompat.getColor(itemView.context, R.color.inapp_civ_badge_stroke_color_start),
                    ContextCompat.getColor(itemView.context, R.color.inapp_civ_badge_stroke_color_end))
                 ).draw()

            imageLoader?.setDefaultLoadingListener(this)

            civInAppIcon.apply {
                isAnimating = true
                borderThickness = 5
                borderColor = ContextCompat.getColor(itemView.context, R.color.inapp_civ_border_color)
                highlightBorderColor = ContextCompat.getColor(itemView.context, R.color.inapp_highlight_border_color)
                highlightBorderColorEnd = ContextCompat.getColor(itemView.context, R.color.inapp_civ_highlight_border_color_end)
                distanceToBorder = 6
                numberOfArches = 5
                totalArchesDegreeArea = 90f
            }
        }

       override fun onLoadingStarted(imageUri: String?, view: View?) {

       }

       override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
           civInAppIcon.isAnimating = false
           InAppLogDebug.d("IMAGE_LOADING_COMPLETED", imageUri)
       }

       override fun onLoadingCancelled(imageUri: String?, view: View?) {
       }

       override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {
           civInAppIcon.isAnimating = false
       }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(layoutResId, viewGroup, false))
    }


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        var model = inAppModel[position]
        var holder = viewHolder as ViewHolder
        holder.tvAppName.text = model.name

        imageLoader?.displayImage(model.icon, holder.civInAppIcon, options)
        /*UrlImageViewHelper.setUrlDrawable(holder.civInAppIcon, model.icon) { _, _, _, _ ->
            if (holder.civInAppIcon.isAnimating) holder.civInAppIcon.isAnimating = false
        }*/

        holder.civInAppIcon.setOnClickListener {
            InAppAnimationUtil.with(holder.tvAdBadge)
                .scaleX(0.8f)
                .scaleY(0.8f)
                .setDuration(260)
                .commit(object : InAppAnimationListener {
                override fun onAnimationEnd() {
                    super.onAnimationEnd()
                    callToActionClickListenerInApp?.onClick(model.packageName)
                }
            })
        }
    }

    fun setOnCallToActionClickListener(listenerInApp: InAppOnCallToActionClickListener?) {
        callToActionClickListenerInApp = listenerInApp
    }

    fun setItems(items: ArrayList<InAppModel>){
        for (item in items){
            add(item)
        }
    }

    fun setItems(items: InAppModel){
         add(items)
    }

     private fun add(item: InAppModel){
        if (item.available && item.packageName != context!!.packageName){
            inAppModel.add(item)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return inAppModel.size
    }
}