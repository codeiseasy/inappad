package com.codeiseasydev.inapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasydev.inapp.InAppRecommended
import com.codeiseasydev.inapp.R
import com.codeiseasydev.inapp.listener.InAppOnCallToActionClickListener
import com.codeiseasydev.inapp.model.InAppModel
import com.codeiseasydev.inapp.theme.InAppRecommendedStyles
import com.codeiseasydev.inapp.util.InAppContextCompat
import com.codeiseasydev.inapp.util.InAppUtils
import java.util.*
import kotlin.collections.ArrayList


class InAppVerticalAdapter(
    private val context: Context?,
    private val styles: InAppRecommendedStyles?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var inAppModelList: ArrayList<InAppModel> = ArrayList()
    private var btnInstallClickListenerInApp: InAppOnCallToActionClickListener? = null

    fun setOnButtonInstallClickListener(listenerInApp: InAppOnCallToActionClickListener) {
        btnInstallClickListenerInApp = listenerInApp
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var recommendedView: InAppRecommended = itemView.findViewById(R.id.inapp_recommended)

        init {
            recommendedView.setOnCallToActionClickListener(View.OnClickListener {
                var inAppModel = inAppModelList[adapterPosition]
                if (inAppModel.packageName != null) {
                    btnInstallClickListenerInApp?.onClick(inAppModel.packageName)
                }
            })

            recommendedView.setOnClickListener {
                var inAppModel = inAppModelList[adapterPosition]
                if (inAppModel.packageName != null) {
                    btnInstallClickListenerInApp?.onClick(inAppModel.packageName)
                }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.inapp_recommended, viewGroup, false)
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        var inAppModel = inAppModelList[position]
        var holder = viewHolder as ViewHolder
        holder.recommendedView.setName(inAppModel.name)
        holder.recommendedView.setDeveloper(inAppModel.developer)
        holder.recommendedView.setReview(inAppModel.ratedStars.toFloat())
        holder.recommendedView.setInstalls(inAppModel.installs)
        holder.recommendedView.setIcon(inAppModel.icon)
        holder.recommendedView.setStyles(styles)
        holder.recommendedView.apply()
    }

    fun setItems(items: ArrayList<InAppModel>) {
        for (item in items) {
            add(item)
        }
    }

    fun setItems(items: InAppModel) {
        add(items)
    }

    private fun add(item: InAppModel) {
        if (item.available && item.packageName != context!!.packageName) {
            inAppModelList.add(item)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return inAppModelList.size
    }
}