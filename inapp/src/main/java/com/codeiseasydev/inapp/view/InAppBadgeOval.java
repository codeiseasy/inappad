package com.codeiseasydev.inapp.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;

import android.util.DisplayMetrics;
import android.util.TypedValue;

import androidx.annotation.ColorInt;


public class InAppBadgeOval {

    Context context;
    DisplayMetrics metrics;
    private float strokeWidth = 1.5F;
    private int[] strokeColors;
    private int badgeBackgroundColor;

    public InAppBadgeOval(Context context) {
        this.context = context;
        this.metrics = context.getResources().getDisplayMetrics();
        this.strokeColors = new int[] {Color.BLACK, Color.BLACK};
        this.badgeBackgroundColor = Color.WHITE;
        //this.strokeColors = new int[] {context.getResources().getColor(R.color.pink400), context.getResources().getColor(R.color.pinkA700)};
    }

    public InAppBadgeOval setStrokeWidth(float width){
        this.strokeWidth = width;
        return this;
    }

    public InAppBadgeOval setStrokeColors(@ColorInt int[] colors){
        this.strokeColors = colors;
        return this;
    }

    public InAppBadgeOval setBadgeBackgroundColor(int bg){
        this.badgeBackgroundColor = bg;
        return this;
    }

    private float dimension(float size){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, metrics);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public Drawable draw(){
        GradientDrawable border = new GradientDrawable();

        border.setColors(strokeColors);
        border.setShape(GradientDrawable.OVAL);
        border.setGradientRadius(12);
        border.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        GradientDrawable background = new GradientDrawable();
        background.setColor(this.badgeBackgroundColor);
        background.setShape(GradientDrawable.OVAL);
        background.setGradientRadius(12);
        border.setGradientType(GradientDrawable.LINEAR_GRADIENT);


        Drawable[] layers = {border, background};
        LayerDrawable layerDrawable = new LayerDrawable(layers);

        layerDrawable.setLayerInset(0, 0, 0, 0, 0);
        layerDrawable.setLayerInset(1, stokeSize(), stokeSize(), stokeSize(), stokeSize());

        return layerDrawable;
    }

    private int stokeSize(){
        return (int) dimension(strokeWidth);
    }
}
