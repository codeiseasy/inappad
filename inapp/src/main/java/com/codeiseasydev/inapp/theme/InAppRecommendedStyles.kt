package com.codeiseasydev.inapp.theme

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat
import com.codeiseasydev.inapp.R
import com.codeiseasydev.inapp.util.InAppContextCompat
import java.lang.Exception

class InAppRecommendedStyles(val context: Context?) {
    private var recommendedNameTextTypeface: Typeface? = null
    private var recommendedNameTextSize: Float = InAppContextCompat.getDimensionToPixel(context, R.dimen.inapp_name_text_size)
    private var recommendedNameTextColor: Int = InAppContextCompat.getColor(context, R.color.inapp_name_text_color)

    private var recommendedDeveloperTextTypeface: Typeface? = null
    private var recommendedDeveloperTextSize: Float = InAppContextCompat.getDimensionToPixel(context, R.dimen.inapp_developer_text_size)
    private var recommendedDeveloperTextColor: Int = InAppContextCompat.getColor(context, R.color.inapp_developer_text_color)

    private var recommendedPriceTextTypeface: Typeface? = null
    private var recommendedPriceTextSize: Float = InAppContextCompat.getDimensionToPixel(context, R.dimen.inapp_price_text_size)
    private var recommendedPriceTextColor: Int = InAppContextCompat.getColor(context, R.color.inapp_price_text_color)

    private var recommendedBadgeTextTypeface: Typeface? = null
    private var recommendedBadgeTextSize: Float = InAppContextCompat.getDimensionToPixel(context, R.dimen.inapp_badge_text_size)
    private var recommendedBadgeTextColor: Int = InAppContextCompat.getColor(context, R.color.inapp_badge_text_color)

    private var recommendedCallToActionBackground: Drawable? = InAppContextCompat.getDrawable(context, R.drawable.inapp_call_to_action_background)
    private var recommendedCallToActionTextTypeface: Typeface? = null
    private var recommendedCallToActionTextSize: Float = InAppContextCompat.getDimensionToPixel(context, R.dimen.inapp_call_to_action_text_size)
    private var recommendedCallToActionTextColor: Int = InAppContextCompat.getColor(context, R.color.inapp_call_to_action_text_color)


    fun getRecommendedNameTextTypeface(): Typeface? {
        return recommendedNameTextTypeface
    }

    fun getRecommendedDeveloperTextTypeface(): Typeface? {
        return recommendedDeveloperTextTypeface
    }

    fun getRecommendedPriceTextTypeface(): Typeface? {
        return recommendedPriceTextTypeface
    }

    fun getRecommendedBadgeTextTypeface(): Typeface? {
        return recommendedBadgeTextTypeface
    }

    fun getRecommendedCallToActionTextTypeface(): Typeface? {
        return recommendedCallToActionTextTypeface
    }


    fun getRecommendedNameTextSize(): Float {
        return recommendedNameTextSize
    }

    fun getRecommendedDeveloperTextSize(): Float {
        return recommendedDeveloperTextSize
    }

    fun getRecommendedPriceTextSize(): Float {
        return recommendedPriceTextSize
    }

    fun getRecommendedBadgeTextSize(): Float {
        return recommendedBadgeTextSize
    }

    fun getRecommendedCallToActionTextSize(): Float {
        return recommendedCallToActionTextSize
    }

    fun getRecommendedNameTextColor(): Int {
        return recommendedNameTextColor
    }

    fun getRecommendedDeveloperTextColor(): Int {
        return recommendedDeveloperTextColor
    }

    fun getRecommendedPriceTextColor(): Int {
        return recommendedPriceTextColor
    }

    fun getRecommendedBadgeTextColor(): Int {
        return recommendedBadgeTextColor
    }

    fun getRecommendedCallToActionTextColor(): Int {
        return recommendedCallToActionTextColor
    }

    fun getRecommendedCallToActionBackground(): Drawable? {
        return recommendedCallToActionBackground
    }


    class Builder {
        private var content: Context? = null
        private var styles: InAppRecommendedStyles? = null

        constructor(ctx: Context?){
            this.content = ctx
            this.styles = InAppRecommendedStyles(content)
        }

        fun withNameTextTypeface(fontPath: String?): Builder{
            try {
                this.styles?.recommendedNameTextTypeface = applyTypeface(fontPath)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withNameTextTypeface(font: Int): Builder{
            try {
                this.styles?.recommendedNameTextTypeface = applyTypeface(font)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withNameTextSize(size: Float): Builder{
            this.styles?.recommendedNameTextSize = size
            return this
        }

        fun withNameTextColor(color: Int): Builder{
            this.styles?.recommendedNameTextColor = color
            return this
        }


        fun withDeveloperTextTypeface(fontPath: String?): Builder{
            try {
                this.styles?.recommendedDeveloperTextTypeface = applyTypeface(fontPath)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withDeveloperTextTypeface(font: Int): Builder{
            try {
                this.styles?.recommendedDeveloperTextTypeface = applyTypeface(font)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withDeveloperTextSize(size: Float): Builder{
            this.styles?.recommendedDeveloperTextSize = size
            return this
        }

        fun withDeveloperTextColor(color: Int): Builder{
            this.styles?.recommendedDeveloperTextColor = color
            return this
        }

        fun withPriceTextTypeface(fontPath: String?): Builder{
            try {
                this.styles?.recommendedPriceTextTypeface = applyTypeface(fontPath)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withPriceTextTypeface(font: Int): Builder{
            try {
                this.styles?.recommendedPriceTextTypeface = applyTypeface(font)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withPriceTextSize(size: Float): Builder{
            this.styles?.recommendedPriceTextSize = size
            return this
        }

        fun withPriceTextColor(color: Int): Builder{
            this.styles?.recommendedPriceTextColor = color
            return this
        }


        fun withBadgeTextTypeface(fontPath: String?): Builder{
            try {
                this.styles?.recommendedBadgeTextTypeface = applyTypeface(fontPath)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withBadgeTextTypeface(font: Int): Builder{
            try {
                this.styles?.recommendedBadgeTextTypeface = applyTypeface(font)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withBadgeTextSize(size: Float): Builder{
            this.styles?.recommendedBadgeTextSize = size
            return this
        }

        fun withBadgeTexColor(color: Int): Builder{
            this.styles?.recommendedBadgeTextColor = color
            return this
        }


        fun withCallToActionTextTypeface(fontPath: String?): Builder{
            try {
                this.styles?.recommendedCallToActionTextTypeface = applyTypeface(fontPath)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withCallToActionTextTypeface(font: Int): Builder{
            try {
                this.styles?.recommendedCallToActionTextTypeface = applyTypeface(font)
            } catch (e: Exception){
                e.printStackTrace()
            }
            return this
        }

        fun withCallToActionTextSize(size: Float): Builder{
            this.styles?.recommendedCallToActionTextSize = size
            return this
        }

        fun withCallToActionTexColor(color: Int): Builder{
            this.styles?.recommendedCallToActionTextColor = color
            return this
        }

        fun withCallToActionBackground(background: Drawable?): Builder{
            this.styles?.recommendedCallToActionBackground = background
            return this
        }

        fun withAllTextTypeface(fontPath: String?): Builder{
            this.styles?.recommendedNameTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedDeveloperTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedPriceTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedBadgeTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedCallToActionTextTypeface = applyTypeface(fontPath)
            return this
        }

        fun withAllTextTypeface(fontPath: Int): Builder{
            this.styles?.recommendedNameTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedDeveloperTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedPriceTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedBadgeTextTypeface = applyTypeface(fontPath)
            this.styles?.recommendedCallToActionTextTypeface = applyTypeface(fontPath)
            return this
        }

        private fun applyTypeface(path: String?): Typeface{
            return Typeface.createFromAsset(content!!.assets, path)
        }

        private fun applyTypeface(font: Int): Typeface? {
            return ResourcesCompat.getFont(content!!, font)
        }

        fun build(): InAppRecommendedStyles? {
            return this.styles
        }
    }


    companion object {

    }
}