package com.codeiseasydev.inapp.client.httpcomponents;


import java.io.Serializable;

/**
 * Basic implementation of {@link InAppNameValuePair}.
 *
 * @since 4.0
 */
@InAppContract(threading = InAppThreadingBehavior.IMMUTABLE)
public class InAppBasicNameValuePair implements InAppNameValuePair, Cloneable, Serializable {

    private static final long serialVersionUID = -6437800749411518984L;

    private final String name;
    private final String value;

    /**
     * Default Constructor taking a name and a value. The value may be null.
     *
     * @param name The name.
     * @param value The value.
     */
    public InAppBasicNameValuePair(final String name, final String value) {
        super();
        this.name = InAppArgs.notNull(name, "Name");
        this.value = value;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        // don't call complex default formatting for a simple toString

        if (this.value == null) {
            return name;
        }
        final int len = this.name.length() + 1 + this.value.length();
        final StringBuilder buffer = new StringBuilder(len);
        buffer.append(this.name);
        buffer.append("=");
        buffer.append(this.value);
        return buffer.toString();
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object instanceof InAppNameValuePair) {
            final InAppBasicNameValuePair that = (InAppBasicNameValuePair) object;
            return this.name.equals(that.name)
                    && InAppLangUtils.equals(this.value, that.value);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = InAppLangUtils.HASH_SEED;
        hash = InAppLangUtils.hashCode(hash, this.name);
        hash = InAppLangUtils.hashCode(hash, this.value);
        return hash;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
