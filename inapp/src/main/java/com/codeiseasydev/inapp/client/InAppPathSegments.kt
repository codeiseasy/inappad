package com.codeiseasydev.inapp.client

object InAppPathSegments {
    val pathSelectAll = arrayOf("api", "apps")
    val pathSelectRecommended = arrayOf("api", "recommended")

    val pathSegmentAppsViews = arrayOf("api", "apps", "views")
    val pathSegmentAppsClicks = arrayOf("api", "apps", "clicks")

    val pathSegmentRecommendedViews = arrayOf("api", "recommended", "views")
    val pathSegmentRecommendedClicks = arrayOf("api", "recommended", "clicks")
}