package com.codeiseasydev.inapp.client

import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.codeiseasydev.inapp.client.httpcomponents.InAppURIBuilder
import com.codeiseasydev.inapp.model.InAppModel
import com.codeiseasydev.inapp.util.InAppLogDebug
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.net.URI
import java.nio.charset.Charset
import java.util.regex.Pattern

class InAppRequestServerClient {
    private var context: Context? = null
    private var serverUrl: String? = null
    private lateinit var pathSegments: Array<out String>
    private var queryName: String? = ""
    private var queryValue: String? = ""
    private var identifier: String? = ""

    private val htmlPattern = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>"
    private val pattern = Pattern.compile(htmlPattern)

    constructor(context: Context?) {
        this.context = context
    }


    fun setUrl(url: String?): InAppRequestServerClient {
        this.serverUrl = url
        return this
    }

    fun setPaths(vararg paths: String): InAppRequestServerClient {
        pathSegments = paths
        return this
    }

    fun setQuery(query: String?, value: String?): InAppRequestServerClient {
        this.queryName = query
        this.queryValue = value
        return this
    }

    fun fetchAll(inAppHttpClientCallback: InAppHttpClientCallback<ArrayList<InAppModel>>?) {
        val request = JsonArrayRequest(Request.Method.GET, filterSchemaUri(serverUrl, *pathSegments), null, Response.Listener { response ->
            var inAppModel = ArrayList<InAppModel>()
            for (i in 0 until response.length()) {
                try {
                    val jsonObject = Gson().fromJson(response.get(i).toString(), InAppModel::class.java)
                    inAppModel.add(jsonObject)
                } catch (e: JSONException) {
                    inAppHttpClientCallback?.onResponseError(e.message.toString())
                }
            }
            inAppHttpClientCallback?.onResponseSuccess(inAppModel)
        }, Response.ErrorListener { volleyError ->
                inAppHttpClientCallback?.onResponseError(volleyError.message.toString())
        })
        InAppBackendVolley.getInstance(context).addToRequestQueue(request)
    }

    fun fetchRecommended(inAppHttpClientCallback: InAppHttpClientCallback<InAppModel>?) {
        if (TextUtils.isEmpty(serverUrl)){
            Toast.makeText(context, "URL field is empty", Toast.LENGTH_LONG).show() // URL schema not validate
            return
        }
        val url = filterSchemaUri(serverUrl, *pathSegments)
        val request = object : JsonObjectRequest(Method.POST, url, null, Response.Listener { response ->
            try {
                val jsonObject = Gson().fromJson(response.toString(), InAppModel::class.java)
                inAppHttpClientCallback?.onResponseSuccess(jsonObject)
            } catch (e: JSONException) {
                inAppHttpClientCallback?.onResponseError(e.message.toString())
            }
        }, Response.ErrorListener { volleyError ->
            inAppHttpClientCallback?.onResponseError(volleyError.message.toString())
        }) {
            override fun getBody(): ByteArray {
                var jsonBody = JSONObject()
                jsonBody.put("package", context!!.packageName)
                return jsonBody.toString().toByteArray(Charset.forName("utf-8"))
            }
        }
        InAppBackendVolley.getInstance(context).addToRequestQueue(request)
    }

    fun identifier(pkg: String?) : InAppRequestServerClient {
        this.identifier = pkg
        return this
    }

    fun views() {
        updateStatistics("views")
    }

    fun clicks() {
      updateStatistics("clicks")
    }

    private fun updateStatistics(body: String){
        val url = filterSchemaUri(serverUrl, *pathSegments)
        val request = object : JsonObjectRequest(Method.POST, url, null, Response.Listener { response ->
            try {
                var jsonObject = JSONObject(response.toString())
                var result = jsonObject.getBoolean("result")
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { volleyError ->
            InAppLogDebug.d("ERROR_VOLLEY", volleyError.message.toString())
        }) {
            override fun getBody(): ByteArray {
                var jsonBody = JSONObject()
                jsonBody.put("identifier", identifier)
                //jsonBody.put(body, 1)
                return jsonBody.toString().toByteArray(Charset.forName("utf-8"))
            }
        }
        InAppBackendVolley.getInstance(context).addToRequestQueue(request)
    }


    private fun filterSchemaUri(url: String?, vararg paths: String): String? {
        if (TextUtils.isEmpty(url)){
            return null
        }
        val uri = URI(url)
        val uriBuilder = InAppURIBuilder()
        uriBuilder.scheme = uri.scheme
        uriBuilder.host = uri.host
        uriBuilder.setPathSegments(*paths)
        val build = uriBuilder.build()
        return build.toURL().toString()
    }

    companion object {
        fun builder(ctx: Context?): InAppRequestServerClient {
            return InAppRequestServerClient(ctx)
        }
    }
}