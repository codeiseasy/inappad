package com.codeiseasydev.inapp.client

import com.codeiseasydev.inapp.model.InAppModel

interface InAppHttpClientCallback<T> {
    //fun onResponseSuccess(inAppModel: ArrayList<InAppModel>)
    fun onResponseSuccess(inAppModel: T)
    fun onResponseError(error: String?)
}
