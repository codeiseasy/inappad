package com.codeiseasydev.inapp.client.httpcomponents;

/**
 Defines types of threading behavior enforced at runtime.
 */
public enum InAppThreadingBehavior {

    /**
     * Instances of classes with the given contract are expected to be fully immutable
     * and thread-safe.
     */
    IMMUTABLE,

    /**
     * Instances of classes with the given contract are expected to be immutable if their
     * dependencies injected at construction time are immutable and are expected to be thread-safe
     * if their dependencies are thread-safe.
     */
    IMMUTABLE_CONDITIONAL,

    /**
     * Instances of classes with the given contract are expected to be fully thread-safe.
     */
    SAFE,

    /**
     * Instances of classes with the given contract are expected to be thread-safe if their
     * dependencies injected at construction time are thread-safe.
     */
    SAFE_CONDITIONAL,

    /**
     * Instances of classes with the given contract are expected to be non thread-safe.
     */
    UNSAFE

}
