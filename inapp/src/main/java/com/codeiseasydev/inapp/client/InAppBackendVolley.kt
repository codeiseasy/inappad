package com.codeiseasydev.inapp.client

import android.content.Context
import android.text.TextUtils
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class InAppBackendVolley {
    private var applicationContext: Context? = null

    constructor(applicationContext: Context?) {
        this.applicationContext = applicationContext
    }


    private val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(applicationContext)
            }
            return field
        }


    fun <T> addToRequestQueue(request: Request<T>, tag: String) {
        request.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue?.add(request)
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        request.tag = TAG
        requestQueue?.add(request)
    }

    fun cancelPendingRequests(tag: Any) {
        if (requestQueue != null) {
            requestQueue!!.cancelAll(tag)
        }
    }

    companion object {
        private val TAG = InAppBackendVolley::class.java.simpleName
        @get:Synchronized var instances: InAppBackendVolley? = null
            private set

        fun getInstance(context: Context?): InAppBackendVolley {
            return InAppBackendVolley(context)
        }
    }
}