package com.codeiseasy.inapp

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasydev.inapp.InAppListApps
import com.codeiseasydev.inapp.InAppRecommended
import com.codeiseasydev.inapp.adapter.InAppHorizontalAdapter
import com.codeiseasydev.inapp.client.InAppHttpClientCallback
import com.codeiseasydev.inapp.client.InAppPathSegments
import com.codeiseasydev.inapp.client.InAppPathSegments.pathSegmentRecommendedClicks
import com.codeiseasydev.inapp.client.InAppPathSegments.pathSegmentRecommendedViews
import com.codeiseasydev.inapp.client.InAppPathSegments.pathSelectAll
import com.codeiseasydev.inapp.client.InAppPathSegments.pathSelectRecommended
import com.codeiseasydev.inapp.client.InAppRequestServerClient
import com.codeiseasydev.inapp.enums.InAppDialogMode
import com.codeiseasydev.inapp.listener.InAppListAppsListener
import com.codeiseasydev.inapp.listener.InAppOnCallToActionClickListener
import com.codeiseasydev.inapp.model.InAppModel
import com.codeiseasydev.inapp.theme.InAppRecommendedStyles
import com.codeiseasydev.inapp.util.InAppConsts.baseURL
import com.codeiseasydev.inapp.util.InAppLogDebug
import com.codeiseasydev.inapp.util.InAppUtils
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    private var btnInappList: Button? = null
    private var recyclerView: RecyclerView? = null
    private var inAppRecommended: InAppRecommended? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.inapp_activity_main)
        btnInappList = findViewById(R.id.btn_inapp_list)
        recyclerView = findViewById(R.id.rv_more_apps)
        inAppRecommended = findViewById(R.id.inapp_recommended)
        /*val policy = StrictMode.ThreadPolicy.Builder()
            .permitAll()
            .build()
        StrictMode.setThreadPolicy(policy)*/
        
        appsListHorizontalRecyclerView()
        btnInappList?.setOnClickListener {
            appsListDialog()
        }
        loadInAppRecommended()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(updateResources(newBase, "en"))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        updateResources(this, "en")
    }

    private fun updateResources(ctx: Context?, language: String): Context? {
        var context = ctx
        var lang = language.substring(0, 2)
        val locale = Locale(lang)
        val res = context!!.resources
        val config = Configuration(res.configuration)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale)
            context = context.createConfigurationContext(config)
        } else {
            config.locale = locale
            res.updateConfiguration(config, res.displayMetrics)
        }
        return context
    }

    fun applyLanguage(lang: String){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            val configuration = resources.configuration
            configuration.setLayoutDirection(Locale(lang))
            resources.updateConfiguration(configuration, resources.displayMetrics)
        }
    }

    private fun loadInAppRecommended(){
        InAppRequestServerClient.builder(this)
            .setUrl("")
            .setPaths(*pathSelectRecommended)
            .fetchRecommended(object : InAppHttpClientCallback<InAppModel> {
                override fun onResponseSuccess(inAppModel: InAppModel) {
                    if (inAppModel.available){
                        InAppRequestServerClient.builder(applicationContext)
                            .setUrl(baseURL)
                            .setPaths(*pathSegmentRecommendedViews)
                            .identifier(packageName)
                            .views()

                        val setterStyles = InAppRecommendedStyles.Builder(applicationContext)
                            .withPriceTextColor(ContextCompat.getColor(applicationContext, R.color.inapp_price_text_color))
                            .build()
                        inAppRecommended?.setName(inAppModel.name)
                        inAppRecommended?.setDeveloper(inAppModel.developer)
                        inAppRecommended?.setReview(inAppModel.ratedStars.toFloat())
                        inAppRecommended?.setInstalls(inAppModel.installs)
                        inAppRecommended?.setIcon(inAppModel.icon)
                        inAppRecommended?.setStyles(setterStyles)
                        inAppRecommended?.apply()
                        inAppRecommended?.setOnCallToActionClickListener(View.OnClickListener {
                            Toast.makeText(applicationContext, "Call To Action Clicked", Toast.LENGTH_LONG).show()
                            InAppRequestServerClient.builder(applicationContext)
                                .setUrl(baseURL)
                                .setPaths(*pathSegmentRecommendedClicks)
                                .identifier(packageName)
                                .clicks()
                            InAppUtils.openAppInPlayStore(this@MainActivity, inAppModel.packageName)

                        })
                        inAppRecommended?.setOnRecommendedClockListener(View.OnClickListener {
                            Toast.makeText(applicationContext, "Recommended view Clicked", Toast.LENGTH_LONG).show()
                            InAppRequestServerClient.builder(applicationContext)
                                .setUrl(baseURL)
                                .setPaths(*pathSegmentRecommendedClicks)
                                .identifier(packageName)
                                .clicks()
                            InAppUtils.openAppInPlayStore(this@MainActivity, inAppModel.packageName)
                        })
                    } else {
                        inAppRecommended?.visibility = View.GONE
                    }
                }
                override fun onResponseError(error: String?) {
                    InAppLogDebug.d("onResponseError", error.toString())
                    inAppRecommended?.visibility = View.GONE
                }
            })
    }

    private fun appsListDialog() {
        InAppListApps.with(this)
            .setUrl("")
            .setPaths(*pathSelectAll)
            .setTitleTypeFace(R.font.zahra_arabic_bold)
            .setTitleTextSize(18f)
            .setStyles(InAppRecommendedStyles.Builder(this)
                .withNameTextTypeface(R.font.zahra_arabic_bold)
                .withDeveloperTextTypeface(R.font.zahra_arabic_bold)
                .withNameTextSize(20f)
                .withDeveloperTextSize(18f)
                .build())
            .setDebugging(BuildConfig.DEBUG)
            .addListener(object : InAppListAppsListener {
                override fun onResponse(response: ArrayList<InAppModel>) {

                }

                override fun onFailure(error: String?) {
                    InAppLogDebug.d("MORE_APPS_RESPONSE_ERROR", error.toString())
                }
            })
            .build(InAppDialogMode.SHEET_BOTTOM)
    }

    private fun appsListHorizontalRecyclerView() {
        var rvMoreApps = InAppHorizontalAdapter(this)
        InAppRequestServerClient.builder(this)
            .setUrl("")
            .setPaths(*pathSelectAll)
            .fetchAll(object : InAppHttpClientCallback<ArrayList<InAppModel>> {
                override fun onResponseSuccess(inAppModel: ArrayList<InAppModel>) {
                    rvMoreApps.setItems(inAppModel)
                    for (app in inAppModel){
                        if (app.available && app.packageName != packageName){
                            InAppRequestServerClient.builder(applicationContext)
                                .setUrl(baseURL)
                                .setPaths(*InAppPathSegments.pathSegmentAppsViews)
                                .identifier(app.packageName)
                                .views()
                            InAppLogDebug.d("RESPONSE_SUCCESS", app.packageName.toString())
                        }
                    }
                }

                override fun onResponseError(error: String?) {
                    InAppLogDebug.d("onResponseError", error.toString())
                }
            })
        recyclerView?.adapter = rvMoreApps
        rvMoreApps.setOnCallToActionClickListener(object : InAppOnCallToActionClickListener {
            override fun onClick(pkg: String?) {
                InAppUtils.openAppInPlayStore(applicationContext, pkg)
                InAppRequestServerClient.builder(applicationContext)
                    .setUrl(baseURL)
                    .setPaths(*InAppPathSegments.pathSegmentAppsClicks)
                    .identifier(pkg)
                    .clicks()
            }
        })
    }

    private fun checkDate(timestamp: Long): Boolean {
        return calcDateViaTimestamp(timestamp).equals(calcDateViaTimestamp(System.currentTimeMillis()), false)
    }

    private fun calcDateViaTimestamp(timestamp: Long) : String {
        var calendar = Calendar.getInstance()
        calendar.timeInMillis = timestamp
        var date = calendar.get(Calendar.DATE)
        var year = calendar.get(Calendar.YEAR)
        var month = calendar.get(Calendar.MONTH)
        return date.toString() + "" + month.toString() + "" + year
    }

}
